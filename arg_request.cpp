﻿/*
Что нужно сделать

Сделайте запрос к сервису httpbin.org, отправив при этом несколько
дополнительных аргументов с данными.
Все названия аргументов и их значения — строковые, и принимаются от
пользователя. Пользователь последовательно вводит их названия 
вместе со значениями, пока не встретится аргумент с названием «post»
или «get», что будет означать, что ввод аргументов закончен и 
требуется отправить их на сервер выбранным способом.
Если пользователь выбрал «post», то выполняется POST-запрос и 
аргументы отправляются POST-способом (как форма). Если «get», 
то GET-запрос с GET-аргументами (прямо в URL).
По результатам выполнения запроса, выведите ответ сервера в 
стандартную консоль и обратите внимание на поля form или args, в 
зависимости от типа запроса в которых должны быть перечислены все 
введённые вами аргументы.

Советы и рекомендации

Согласно HTTP-стандарту, аргументы в GET-запросах перечисляются 
прямо в URL запроса после вопросительного знака. Названия аргументов
отделяются от их значений с помощью символа «=»), а сами аргументы
разделены символами «&».

Пример аргументов, заданных в URL запроса: ?foo=first&animal=cat&bar=third
*/
#include <iostream>
#include <map>
#include <cpr/cpr.h>
#include "arg_request.h"

enum request_type {
    UNKNOWN,
    GET,
    POST
};

std::string new_url(std::map < std::string, std::string>& arguments,
                    std::string& old_url) {

    std::string new_url = old_url + "/get";
    std::map<std::string, std::string>::iterator it;
    if (!arguments.empty()) {
        new_url += "?";
        for (it = arguments.begin(); it != arguments.end(); it++)
        {
            new_url += it->first + '=' + it->second + '&';
        }
        new_url.erase(new_url.end() - 1, new_url.end());
    }
    
    return new_url;    
}

cpr::Payload payload(std::map<std::string, std::string>& arguments) {
    cpr::Payload payload{ {} };
    cpr::CurlHolder ch{};
    std::map<std::string, std::string>::iterator it;
    if (!arguments.empty()) {
        for (it = arguments.begin(); it != arguments.end(); it++) {
            cpr::Pair p{it->first+' ' , it->second.c_str() };
            payload.AddPair(p, ch);
        }
    }
    return payload;
}

int main()
{

    std::string url{ "http://httpbin.org" };

    std::map<std::string, std::string> arguments;
    std::string key;
    std::string value;
    request_type type = UNKNOWN;

    while (true) {
        std::cout << "Please input key: ";
        std::cin >> key;
        if (key == "get") {
            type = GET;
            break;
        }
        else if (key == "post") {
            type = POST;
            break;
        }
        std::cout << "Please input value: ";
        std::cin >> value;
        std::cout << std::endl;
        arguments.insert({ key,value });
    }

    if (type == GET) {
        cpr::Response rg = cpr::Get(cpr::Url(new_url(arguments, url)));
        std::cout << rg.text << std::endl;
    }
    else if (type == POST) {
        url += "/post";
        cpr::Response rp = cpr::Post(cpr::Url(url),
            payload(arguments));
        std::cout << rp.text << std::endl;
    }
    return 0;
}